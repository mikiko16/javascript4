import EventEmitter from 'eventemitter3';
import Species from './Species';

const EVENTS = {
    MAX_SPECIES_REACHED: 'max_species_reached',
    SPECIES_CREATED: 'species_created' }

export default class StarWarsUniverse extends EventEmitter {
    constructor(species = 10) {
        super();
        this._maxSpecies = species;
        this.species = [];

        this.addListener('species_created', function firstEmitter(a) {  
        });

        this.addListener('max_species_reached', function secondEmitter() {           
        });
    }

    static get events() {
        return EVENTS;
    }

    get speciesCount() {
        return this.species.length;
    }

    async createSpecies() {
        let newSpecies = new Species();

        this.emit(StarWarsUniverse.events.SPECIES_CREATED);

        await newSpecies.init('https://swapi.booost.bg/api/species/' + (this.species.length + 1).toString());

        await this._onSpeciesCreated(newSpecies);
    }

    async _onSpeciesCreated(species) {

        this.species.push(species);

        this.emit(StarWarsUniverse.events.SPECIES_CREATED, {speciesCount: this.species.length});

        if (this.species.length === this._maxSpecies) {
            this.emit(StarWarsUniverse.events.MAX_SPECIES_REACHED)
        }
        else {
            await this.createSpecies(); 
        }
    }
}