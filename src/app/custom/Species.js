import EventEmitter from 'eventemitter3';

const EVENTS = {
    SPECIES_CREATED: 'species_created' }

export default class Species extends EventEmitter{
    constructor() {
        super();
        this.name = null;
        this.classification = null;

        this.addListener('species_created', function firstEmitter() {
        });
    }

    static get events() {
        return EVENTS;
    }

    async init(url) {

        var opts = {
            method: 'GET',      
            headers: {}
            };
            
        await fetch(url, opts)
            .then(function (response) {
                return response.json();
            })
            .then(async (body) => {
                await this.setSpecies(body);
        });

        this.emit(Species.events.SPECIES_CREATED);
    }

    async setSpecies(body) {
        this.name = body.name;
        this.classification = body.classification;
    }
}